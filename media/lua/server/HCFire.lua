--Fire Risks

-- Burn the player, for real do it
    -- Helper function for getBurned
        function doBurn(items, result, player)
            local bgt=player:getBodyDamage();
            local bodyParts = {bgt:getBodyPart(BodyPartType.Hand_R),bgt:getBodyPart(BodyPartType.Hand_L),bgt:getBodyPart(BodyPartType.ForeArm_L),bgt:getBodyPart(BodyPartType.ForeArm_R),bgt:getBodyPart(BodyPartType.Torso_Upper),bgt:getBodyPart(BodyPartType.Head)}
            local bodyPartsBias = {25, 50, 63, 75, 88, 100}
            local partRoll = ZombRand(100) -- between 0 and 99
            local bodyPart = bodyParts[0] -- failsafe in case something messes up, we always have a valid burn target

            -- Body part bias works like this:
            -- We have a chance to burn any part of the body. The chances are as follows:
            -- 25% chance of each hand, so 50% chance it'll burn the hands
            -- then 25% chance it'll burn the forearms, split between the two means right forearm has a slightly higher chance to burn
            -- then remaining 25% chance is split between upper torso and head, slight bias towards upper torso for same reason
            -- We determine the part with the partRoll variable, higher numbers mean more 'important' parts get burned
            for count, bodyPart in ipairs(bodyParts) do
                if partRoll < bodyPartsBias[count] then
                    bodyPart = bodyParts[count]
                    break
                end
            end

            -- Now burn the chosen part
            bodyPart:AddDamage(burnPower);
            bodyPart:setBurned();
            player:getBodyDamage():SetBandaged(bodyPart:getIndex(), false, 0, false, nil);

            player:Say("Ouch, that burns!");
            player:getCurrentSquare():playSound("PZ_Fire", false);
        end

        -- Check if we should burn the player and do it
                function getBurned(items, result, player)
                    -- Get protection tier
                    local protectionTier = 0
                    local protectionTier3Parts = 0
                    local buildingTier = 0
                    local inv = player:getInventory():getItems();
                    local item ="";
                    local itemType="";

                    for i = 0, inv:size() -1 do
                        item=inv:get(i);
                        itemType=item:getType();
                        if item:isEquipped() then


                            if itemType == "HCWorkgloves"				then protectionTier=1;
                            elseif itemType == "HCGlovesHardLeather"	then protectionTier=2;protectionTier3Parts=protectionTier3Parts+1;
                            elseif itemType == "Jacket_Fireman"			then protectionTier3Parts=protectionTier3Parts+1;
                            elseif itemType == "HCBlacksmithapron"		then protectionTier3Parts=protectionTier3Parts+1;
                            elseif itemType == "Hat_Fireman" 			then protectionTier3Parts=protectionTier3Parts+1;
                            elseif itemType == "WeldingMask" 			then protectionTier3Parts=protectionTier3Parts+1;
                            elseif itemType == "HCFiresuit"				then protectionTier=5;
                            end --chk clothing item


                        end -- is equiped?
                    end -- loop

                    -- Check if we have all protection tier 3 parts, but don't lower the tier
                        if protectionTier3Parts == 5 and protectionTier < 3 then
                            protectionTier = 3
                        end

                        -- Adjust tier of protection if player has relavent profession
                            if player:getDescriptor():getProfession() == "fireofficer" then protectionTier=protectionTier+1;
                            elseif player:getDescriptor():getProfession() == "metalworker" then protectionTier=protectionTier+1;
                            end

                            -- Get the building's tier
                            if HCNearKiln(player) then buildingTier=1;
                            elseif containsItem ("HCSmelter2",items) 		then buildingTier=2;
                            elseif containsItem ("HCBlastfurnace2",items) 	then buildingTier=3;
                            elseif containsItem ("HCFurnace2",items) 		then buildingTier=4;
                            end

                            -- Should we burn?
                            if protectionTier < buildingTier then
                                -- You have lower tier protection, roll the burns 3 times
                                doBurn(items, result, player)
                                doBurn(items, result, player)
                                doBurn(items, result, player)
                            elseif protectionTier == buildingTier then
                                -- Check if lucky
                                    if not player:getTraits():contains('Lucky') then
                                        if ZombRand(1000) == 0 then
                                            -- You're going to be doing a lot of smelting. 1 in 1000 chance is going to guarentee you're going to get burned.
                                            -- Also ZombRand returns between 0 and (n-1)
                                            doBurn(items, result, player)
                                        end
                                    end
                                end
                            end

                            function KilnUse(items, result, player)

                            end

                            function SmeltUse(items, result, player)

                            end

                            function BFurnUse(items, result, player)

                            end

                            function IFurnUse(items, result, player)

                            end

                            function WandFire(items, result, player)
                                player:setOnFire(true);
                            end
