require "Farming/forageDefinitions"

-- Steal functions from vanilla zomboid since they declare them as local (booooo)
local function iterList(_list)
    local list = _list;
    local size = list:size() - 1;
    local i = -1;
    return function()
        i = i + 1;
        if i <= size and not list:isEmpty() then
            return list:get(i), i;
        end;
    end;
end

local function doDeadTrapAnimalSpawn(_character, _inventory, _itemDef, _items)
    for item in iterList(_items) do
        --search for trap animal definition
        for _, trapDef in pairs(Animals) do
            if trapDef.item == _itemDef.type then
                -- Randomize the hunger reduction of the animal
                local size = ZombRand(trapDef.minSize, trapDef.maxSize);
                local typicalSize = item:getBaseHunger() * -100;
                local statsModifier = size / typicalSize;
                -- Set the animal's stats depending on the random size
                item:setBaseHunger(item:getBaseHunger() * statsModifier);
                item:setHungChange(item:getHungChange() * statsModifier);
                item:setActualWeight(item:getActualWeight() * statsModifier);
                item:setCarbohydrates(item:getCarbohydrates() * statsModifier);
                item:setLipids(item:getLipids() * statsModifier);
                item:setProteins(item:getProteins() * statsModifier);
                item:setCalories(item:getCalories() * statsModifier);
            end;
        end;
        --dead animals can only be in the range of barely fresh to rotted
        if ZombRand(10) + 1 <= 3 then
            --barely fresh to rotted
            local freshLimit = item:getOffAge() - (item:getOffAge() / 4);
            item:setAge(ZombRandFloat(freshLimit, item:getOffAgeMax()));
        else
            --stale to rotted
            item:setAge(ZombRandFloat(item:getOffAge(), item:getOffAgeMax()));
        end;
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

forageDefs.HCSparrowdead = {
    type = "Hydrocraft.HCSparrowdead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
    forceOutside = false,
    canBeAboveFloor = true,
}

forageDefs.HCPigeondead = {
    type = "Hydrocraft.HCPigeondead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
    forceOutside = false,
    canBeAboveFloor = true,
}

forageDefs.HCQuaildead = {
    type = "Hydrocraft.HCQuaildead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCGrousedead = {
    type = "Hydrocraft.HCGrousedead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCTurkeydead = {
    type = "Hydrocraft.HCTurkeydead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCChickendead = {
    type = "Hydrocraft.HCChickendead",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCChickendead2 = {
    type = "Hydrocraft.HCChickendead2",
    skill = 7,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 2,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCMuskratdead = {
    type = "Hydrocraft.HCMuskratdead",
    skill = 8,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCGrousedead = {
    type = "Hydrocraft.HCGroundhogdead",
    skill = 8,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCNutriadead = {
    type = "Hydrocraft.HCNutriadead",
    skill = 8,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCBeaverdead = {
    type = "Hydrocraft.HCBeaverdead",
    skill = 10,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCOpossumdead = {
    type = "Hydrocraft.HCOpossumdead",
    skill = 10,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 1,
        DeepForest  = 1,
        Vegitation  = 2,
        FarmLand    = 2,
        Farm        = 2,
        TrailerPark = 0,
        TownZone    = 0,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
    forceOutside = false,
    canBeAboveFloor = true,
}

forageDefs.HCSkunkdead = {
    type = "Hydrocraft.HCSkunkdead",
    skill = 10,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 2,
        DeepForest  = 2,
        Vegitation  = 2,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 2,
        TownZone    = 2,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCRaccoondead = {
    type = "Hydrocraft.HCRaccoondead",
    skill = 10,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 1,
        DeepForest  = 1,
        Vegitation  = 1,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 1,
        TownZone    = 1,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}

forageDefs.HCFoxdead = {
    type = "Hydrocraft.HCRaccoondead",
    skill = 10,
    xp = 10,
    rainChance = -10,
    snowChance = -10,
    categories = { "DeadAnimals" },
    zones = {
        Forest      = 1,
        DeepForest  = 1,
        Vegitation  = 1,
        FarmLand    = 1,
        Farm        = 1,
        TrailerPark = 1,
        TownZone    = 1,
        Nav         = 1,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doDeadTrapAnimalSpawn },
}
