require "Farming/forageDefinitions"

forageDefs.HCCoal = {
    type = "Hydrocraft.HCCoal",
    minCount = 1,
    maxCount = 2,
    snowChance = -50,
    skill = 5,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCGreyclay = {
    type = "Hydrocraft.HCGreyclay",
    minCount = 1,
    maxCount = 4,
    snowChance = -50,
    skill = 1,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCRedclay= {
    type = "Hydrocraft.HCRedclay",
    minCount = 1,
    maxCount = 4,
    snowChance = -50,
    skill = 1,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCCopperore = {
    type = "Hydrocraft.HCCopperore",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 6,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCTinore = {
    type = "Hydrocraft.HCTinore",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 6,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCIronore = {
    type = "Hydrocraft.HCIronore",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 7,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCLeadore = {
    type = "Hydrocraft.HCLeadore",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 7,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCSaltpeter = {
    type = "Hydrocraft.HCSaltpeter",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 7,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCSulphur = {
    type = "Hydrocraft.HCSulphur",
    minCount = 1,
    maxCount = 3,
    snowChance = -50,
    skill = 7,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        Vegitation = 1,
        FarmLand = 1,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCBauxiteore = {
    type = "Hydrocraft.HCBauxiteore",
    minCount = 1,
    maxCount = 2,
    snowChance = -50,
    skill = 8,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 2,
        DeepForest = 2,
        Vegitation = 0,
        FarmLand = 0,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCLithiumore = {
    type = "Hydrocraft.HCLithiumore",
    minCount = 1,
    maxCount = 2,
    snowChance = -50,
    skill = 9,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 2,
        DeepForest = 2,
        Vegitation = 0,
        FarmLand = 0,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCSilverore = {
    type = "Hydrocraft.HCSilverore",
    minCount = 1,
    maxCount = 2,
    snowChance = -50,
    skill = 9,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 2,
        DeepForest = 2,
        Vegitation = 0,
        FarmLand = 0,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

forageDefs.HCGoldore = {
    type = "Hydrocraft.HCGoldore",
    minCount = 1,
    maxCount = 2,
    snowChance = -50,
    skill = 10,
    xp = 5,
    categories = { "Stones" },
    zones = {
        Forest = 1,
        DeepForest = 1,
        Vegitation = 0,
        FarmLand = 0,
        TrailerPark = 0,
        TownZone = 0,
        Nav = 0,
    },
    months = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
}

