require "Farming/forageDefinitions"

-- Steal functions from vanilla zomboid since they declare them as local (booooo)
local worldSprites = {
    wildPlants = {
        { "d_plants_1_11" },
        { "d_plants_1_12" },
        { "d_plants_1_13" },
        { "d_plants_1_14" },
        { "d_plants_1_15" },
    },
    smallTrees = {
        { "media/textures/Foraging/worldSprites/smallTree_worldSprite.png" },
        { "media/textures/Foraging/worldSprites/smallTree2_worldSprite.png" },
    },
    berryBushes = {
        { "f_bushes_1_4", "f_bushes_1_68", "f_bushes_1_84" },
        { "f_bushes_1_4", "f_bushes_1_68", "f_bushes_1_88" },
    },
    bushes = {
        { "f_bushes_1_64" },
        { "f_bushes_1_65" },
        { "f_bushes_1_66" },
    },
    shrubs = {
        { "d_plants_1_19" },
        { "d_plants_1_20" },
        { "d_plants_1_21" },
        { "d_plants_1_22" },
        { "d_plants_1_23" },
        { "f_bushes_1_68" },
        { "f_bushes_1_77" },
        { "f_bushes_1_78" },
    },
    vines = {
        { "d_plants_1_38" },
        { "d_plants_1_49" },
        { "d_plants_1_50" },
    },
};

local function iterList(_list)
    local list = _list;
    local size = list:size() - 1;
    local i = -1;
    return function()
        i = i + 1;
        if i <= size and not list:isEmpty() then
            return list:get(i), i;
        end;
    end;
end

local function doWildFoodSpawn(_character, _inventory, _itemDef, _items)
    -- Randomize the size of the item
    local perkLevel = forageSystem.getPerkLevel(_character, _itemDef);
    for item in iterList(_items) do
        item:setName(item:getDisplayName() .. " (" .. getText("UI_foraging_WildFood") .. ")");
        --25 to 75 percent size
        local sizeModifier = ((ZombRand(50) + 25) / 100);
        --add up to 50% for perk level
        sizeModifier = sizeModifier + ((ZombRand(perkLevel) + 1) / 5)
        -- Set the item's stats depending on the random size
        if item:getBaseHunger() <= -0.02 then
            item:setBaseHunger(item:getBaseHunger() * sizeModifier);
            item:setHungChange(item:getHungChange() * sizeModifier);
            item:setCarbohydrates(item:getCarbohydrates() * sizeModifier);
            item:setLipids(item:getLipids() * sizeModifier);
            item:setProteins(item:getProteins() * sizeModifier);
            item:setCalories(item:getCalories() * sizeModifier);
            item:setUnhappyChange(item:getUnhappyChange() * sizeModifier);
            --item:multiplyFoodValues(sizeModifier);
            --item:setActualWeight(item:getActualWeight() * sizeModifier);
        end;
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

local function doRandomAgeSpawn(_character, _inventory, _itemDef, _items)
    -- Randomize the size of the item
    local perkLevel = forageSystem.getPerkLevel(_character, _itemDef);
    for item in iterList(_items) do
        --set random age based on perkLevel and random chance
        local randomAge = 0;
        if (ZombRand(10) + 1) <= perkLevel then
            randomAge = ZombRandFloat(0.0, item:getOffAge() / 2);
        elseif ZombRand(3) <= perkLevel then
            randomAge = ZombRandFloat(0.0, item:getOffAge());
        else
            randomAge = ZombRandFloat(item:getOffAge(), item:getOffAgeMax());
        end;
        item:setAge(randomAge);
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

local function doPoisonItemSpawn(_character, _inventory, _itemDef, _items)
    if _itemDef.poisonChance and _itemDef.poisonPowerMin and _itemDef.poisonPowerMax then
        if _itemDef.poisonChance > 0 and _itemDef.poisonPowerMax > 0 then
            local perkLevel = forageSystem.getPerkLevel(_character, _itemDef);
            --increase poison chance by up to 30% for level 0
            if ZombRand(100) < _itemDef.poisonChance + ((10 - perkLevel) * 3) then
                for item in iterList(_items) do
                    item:setPoisonPower(ZombRand(_itemDef.poisonPowerMin, _itemDef.poisonPowerMax) + 1);
                    item:setPoisonDetectionLevel(_itemDef.poisonDetectionLevel or 0);
                    item:setUseForPoison(item:getHungChange());
                end;
            end;
        end;
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

local function doGenericItemSpawn(_character, _inventory, _itemDef, _items)
    for item in iterList(_items) do
        if item:IsDrainable() then
            item:setUsedDelta(ZombRandFloat(0.0, 1.0)); -- Randomize the item uses remaining
        end;
        local conditionMax = item:getConditionMax();
        if conditionMax > 0 then
            item:setCondition(ZombRand(conditionMax) + 1); -- Randomize the weapon condition
        end;
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

local function doClothingItemSpawn(_character, _inventory, _itemDef, _items)
    for item in iterList(_items) do
        if not item:isCosmetic() then
            item:setWetness(ZombRandFloat(0.0, 100.0));
            item:setBloodLevel(ZombRandFloat(0.0, 100.0));
            item:setDirtyness(ZombRandFloat(0.0, 100.0));
            local conditionMax = item:getConditionMax();
            if conditionMax > 0 then
                item:setCondition(ZombRand(conditionMax) + 1);
            end;
        end;
    end;
    return _items; --custom spawn scripts must return an arraylist of items (or nil)
end

forageDefs.HCCandleberry = {
    type = "Hydrocraft.HCCandleberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCHuckleberry = {
    type = "Hydrocraft.HCHuckleberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCCranberry= {
    type = "Hydrocraft.HCCranberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCSeaberry = {
    type = "Hydrocraft.HCSeaberry",
    minCount = 2,
    maxCount = 5,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCBlackcurrant = {
    type = "Hydrocraft.HCBlackcurrant",
    minCount = 2,
    maxCount = 5,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCRedcurrant = {
    type = "Hydrocraft.HCRedcurrant",
    minCount = 2,
    maxCount = 5,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCArrowwoodberry = {
    type = "Hydrocraft.HCArrowwoodberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCRaspberry = {
    type = "Hydrocraft.HCRaspberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCElderberry = {
    type = "Hydrocraft.HCElderberry",
    minCount = 2,
    maxCount = 5,
    skill = 2,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCHolly = {
    type = "Hydrocraft.HCHolly",
    minCount = 2,
    maxCount = 5,
    skill = 2,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCBlackthornberry = {
    type = "Hydrocraft.HCBlackthornberry",
    minCount = 2,
    maxCount = 5,
    skill = 9,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCCrampbarkberry = {
    type = "Hydrocraft.HCCrampbarkberry",
    minCount = 2,
    maxCount = 5,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCCrowberry = {
    type = "Hydrocraft.HCCrowberry",
    minCount = 2,
    maxCount = 5,
    skill = 9,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCGooseberry = {
    type = "Hydrocraft.HCGooseberry",
    minCount = 2,
    maxCount = 5,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCHawthornfruit = {
    type = "Hydrocraft.HCHawthornfruit",
    minCount = 2,
    maxCount = 5,
    skill = 9,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCJuniperberry = {
    type = "Hydrocraft.HCJuniperberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCSumacberry = {
    type = "Hydrocraft.HCSumacberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 5,
        DeepForest = 5,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9 },
    bonusMonths = { 6, 7, 8 },
    malusMonths = { 5, 9 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCGrass = {
    type = "Hydrocraft.HCGrass",
    minCount = 2,
    maxCount = 6,
    skill = 0,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCReeds = {
    type = "Hydrocraft.HCReeds",
    minCount = 2,
    maxCount = 6,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFlax = {
    type = "Hydrocraft.HCFlax",
    minCount = 2,
    maxCount = 6,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 10,
        DeepForest = 10,
        Vegitation = 10,
        FarmLand = 7,
        Farm = 7,
        TrailerPark = 5,
        TownZone = 5,
        Nav = 5,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCStraw = {
    type = "Hydrocraft.HCStraw",
    minCount = 2,
    maxCount = 6,
    skill = 0,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCMilkweedroot = {
    type = "Hydrocraft.HCMilkweedroot",
    minCount = 2,
    maxCount = 6,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCClover = {
    type = "Hydrocraft.HCClover",
    minCount = 2,
    maxCount = 6,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCNettles = {
    type = "Hydrocraft.HCNettles",
    minCount = 2,
    maxCount = 6,
    skill = 4,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCThistle = {
    type = "Hydrocraft.HCThistle",
    minCount = 2,
    maxCount = 6,
    skill = 4,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCJutestems = {
    type = "Hydrocraft.HCJutestems",
    minCount = 2,
    maxCount = 6,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCValerian = {
    type = "Hydrocraft.HCValerian",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCRosebud = {
    type = "Hydrocraft.HCRosebud",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCSunflower = {
    type = "Hydrocraft.HCSunflower",
    minCount = 1,
    maxCount = 2,
    skill = 6,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCVenusflytrap = {
    type = "Hydrocraft.HCVenusflytrap",
    minCount = 1,
    maxCount = 2,
    skill = 6,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCCamomile = {
    type = "Hydrocraft.HCVenusflytrap",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCLavender = {
    type = "Hydrocraft.HCLavender",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFlaxflower = {
    type = "Hydrocraft.HCFlaxflower",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCDaisy = {
    type = "Hydrocraft.HCDaisy",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCIris = {
    type = "Hydrocraft.HCIris",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCSkullcap = {
    type = "Hydrocraft.HCSkullcap",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCBluebell = {
    type = "Hydrocraft.HCBluebell",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCBark = {
    type = "Hydrocraft.HCBark",
    minCount = 1,
    maxCount = 2,
    skill = 0,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCBirchbark = {
    type = "Hydrocraft.HCBirchbark",
    minCount = 1,
    maxCount = 2,
    skill = 0,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCWillowbark = {
    type = "Hydrocraft.HCWillowbark",
    minCount = 1,
    maxCount = 2,
    skill = 0,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFircone = {
    type = "Hydrocraft.HCFircone",
    minCount = 1,
    maxCount = 2,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCBirchcatkin = {
    type = "Hydrocraft.HCBirchcatkin",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCBeechnut = {
    type = "Hydrocraft.HCBeechnut",
    minCount = 1,
    maxCount = 2,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCChestnut = {
    type = "Hydrocraft.HCChestnut",
    minCount = 1,
    maxCount = 2,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCHickorynuts = {
    type = "Hydrocraft.HCHickorynuts",
    minCount = 1,
    maxCount = 2,
    skill = 2,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCHickoryleaves = {
    type = "Hydrocraft.HCHickoryleaves",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCOakleaves = {
    type = "Hydrocraft.HCOakleaves",
    minCount = 1,
    maxCount = 1,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCMapleleaf = {
    type = "Hydrocraft.HCMapleleaf",
    minCount = 2,
    maxCount = 4,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFern = {
    type = "Hydrocraft.HCFern",
    minCount = 2,
    maxCount = 4,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFiddleheadfern = {
    type = "Hydrocraft.HCFiddleheadfern",
    minCount = 2,
    maxCount = 4,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "WildPlants" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCWillowbranch = {
    type = "Hydrocraft.HCWillowbranch",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCAlderbough = {
    type = "Hydrocraft.HCAlderbough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCElmbough = {
    type = "Hydrocraft.HCElmbough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCSprucebough = {
    type = "Hydrocraft.HCSprucebough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCSprucebough = {
    type = "Hydrocraft.HCSprucebough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCYewbough = {
    type = "Hydrocraft.HCYewbough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCPinebough = {
    type = "Hydrocraft.HCPinebough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCFirbough = {
    type = "Hydrocraft.HCFirbough",
    minCount = 1,
    maxCount = 2,
    skill = 1,
    xp = 1,
    snowChance = -50,
    categories = { "Firewood" },
    zones = {
        Forest = 20,
        DeepForest = 20,
        Vegitation = 20,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 10,
        TownZone = 10,
        Nav = 10,
    },
    bonusMonths = { 9, 10, 11 },
    malusMonths = { 12, 1, 2 },
}

forageDefs.HCCrabapple = {
    type = "Hydrocraft.HCCrabapple",
    minCount = 1,
    maxCount = 1,
    skill = 10,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 10,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9, 10, 11, 12 },
    bonusMonths = { 7, 8, 9, 10 },
    malusMonths = { 5, 12 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

-- Next would be cabbage, carrot, corn, grapes, lemon, orange, potato, redradish, tomato, but its already in vanilla
forageDefs.HCMulberryleaf = {
    type = "Hydrocraft.HCMulberryleaf",
    minCount = 1,
    maxCount = 2,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 10,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9, 10, 11, 12 },
    bonusMonths = { 7, 8, 9, 10 },
    malusMonths = { 5, 12 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCMulberry = {
    type = "Hydrocraft.HCMulberry",
    minCount = 2,
    maxCount = 5,
    skill = 3,
    xp = 5,
    snowChance = -10,
    categories = { "Fruits" },
    zones = {
        Forest = 10,
        FarmLand = 15,
        Farm = 15,
    },
    months = { 5, 6, 7, 8, 9, 10, 11, 12 },
    bonusMonths = { 7, 8, 9, 10 },
    malusMonths = { 5, 12 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
    altWorldTexture = worldSprites.smallTrees,
    itemSizeModifier = 1.0,
    isItemOverrideSize = true,
}

forageDefs.HCBlewitshroom = {
    type = "Hydrocraft.HCBlewitshroom",
    minCount = 1,
    maxCount = 2,
    skill = 5,
    xp = 5,
    rainChance = 15,
    categories = { "Mushrooms" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 15,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 15,
        TownZone = 15,
        Nav = 15,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    bonusMonths = { 8, 9, 10 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
}

forageDefs.HCLobstershroom = {
    type = "Hydrocraft.HCLobstershroom",
    minCount = 1,
    maxCount = 2,
    skill = 7,
    xp = 5,
    rainChance = 15,
    categories = { "Mushrooms" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 15,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 15,
        TownZone = 15,
        Nav = 15,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    bonusMonths = { 8, 9, 10 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
}

forageDefs.HCWitchshatshroom = {
    type = "Hydrocraft.HCWitchshatshroom",
    minCount = 1,
    maxCount = 2,
    skill = 7,
    xp = 5,
    rainChance = 15,
    categories = { "Mushrooms" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 15,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 15,
        TownZone = 15,
        Nav = 15,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    bonusMonths = { 8, 9, 10 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
}

forageDefs.HCYellowmorelshroom = {
    type = "Hydrocraft.HCYellowmorelshroom",
    minCount = 1,
    maxCount = 2,
    skill = 7,
    xp = 5,
    rainChance = 15,
    categories = { "Mushrooms" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 15,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 15,
        TownZone = 15,
        Nav = 15,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    bonusMonths = { 8, 9, 10 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
}

forageDefs.HCChantrelle = {
    type = "Hydrocraft.HCChantrelle",
    minCount = 1,
    maxCount = 2,
    skill = 4,
    xp = 5,
    rainChance = 15,
    categories = { "Mushrooms" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 15,
        FarmLand = 15,
        Farm = 15,
        TrailerPark = 15,
        TownZone = 15,
        Nav = 15,
    },
    months = { 3, 4, 5, 6, 7, 8, 9, 10, 11 },
    bonusMonths = { 8, 9, 10 },
    malusMonths = { 3, 4 },
    spawnFuncs = { doWildFoodSpawn, doRandomAgeSpawn },
}

forageDefs.HCOakLog = {
    type = "Hydrocraft.HCOakLog",
    minCount = 1,
    maxCount = 2,
    skill = 7,
    xp = 5,
    categories = { "Firewood" },
    zones = {
        Forest = 2,
        DeepForest = 3,
        Vegitation = 1,
    },
    bonusMonths = { 9, 10, 11 },
    itemSizeModifier = 5,
    isItemOverrideSize = true,
}

forageDefs.HCCottonseeds = {
    type = "Hydrocraft.HCCottonseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCHempseeds = {
    type = "Hydrocraft.HCHempseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCPoppyseeds = {
    type = "Hydrocraft.HCPoppyseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCRubbertreeseeds = {
    type = "Hydrocraft.HCRubbertreeseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCSpirulinaseeds = {
    type = "Hydrocraft.HCSpirulinaseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCBasilseeds = {
    type = "Hydrocraft.HCBasilseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCOreganoseeds = {
    type = "Hydrocraft.HCOreganoseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCBananaseeds = {
    type = "Hydrocraft.HCBananaseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCOnionseeds = {
    type = "Hydrocraft.HCOnionseeds",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCWheat = {
    type = "Hydrocraft.HCWheat",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}

forageDefs.HCSoyBeans = {
    type = "Hydrocraft.HCSoyBeans",
    minCount = 1,
    maxCount = 6,
    skill = 7,
    xp = 1,
    categories = { "WildPlants" },
    zones = {
        Forest = 15,
        DeepForest = 15,
        Vegitation = 5,
    },
    months = { 8, 9, 10 },
}
