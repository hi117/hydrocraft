## Feature Requested
<!-- What is your idea? !-->

## Benefits and risks
<!-- 
    What benefits does this bring?
        - better gameplay
        - better compatibility with other mods
        - its cool
        
    What risks might this introduce?
        - lag
        - incompatabilities
        - complexity
!-->

## Proposed solution
<!-- Any indeas on how this might be done? !-->
